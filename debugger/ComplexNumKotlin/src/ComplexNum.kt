import java.text.NumberFormat
import java.util.*

class ComplexNum(val real: Double, val imag: Double) : Comparable<ComplexNum>, Number() {
    companion object {
        @JvmStatic
        val i = ComplexNum(0, 1)
    }

    constructor() : this(0.0, 0.0)
    constructor(cn: ComplexNum) : this(cn.real, cn.imag)
    constructor(cn: ComplexNum, length: Number) : this(cn.real * length.toDouble() / cn.abs, cn.imag * length.toDouble() / cn.abs)
    constructor(real: Number, imag: Number) : this(real.toDouble(), imag.toDouble())
    constructor(real: Number) : this(real.toDouble(), 0.0)

    override fun toString(): String {
        val format = NumberFormat.getInstance(Locale.ENGLISH)
        format.maximumFractionDigits = 10
        return if (this == ComplexNum()) "0" else
            "${(if (this.real != 0.0) format.format(this.real) + (if (this.imag > 0) "+" else "") else "")}${if (this.imag != 0.0) format.format(this.imag) + "i" else ""}"
    }

    val conjugate: ComplexNum
        get() = ComplexNum(this.real, -this.imag)
    val abs: Double
        get() = Math.sqrt(Math.pow(this.real, 2.0) + Math.pow(this.imag, 2.0))


    infix operator fun plus(a: ComplexNum) = ComplexNum(this.real + a.real, this.imag + a.imag)
    infix operator fun minus(a: ComplexNum) = ComplexNum(this.real - a.real, this.imag - a.imag)
    infix operator fun plus(a: Number) = ComplexNum(this.real + a.toDouble(), this.imag)
    infix operator fun minus(a: Number) = ComplexNum(this.real - a.toDouble(), this.imag)
    operator fun unaryMinus() = ComplexNum(-this.real, -this.imag)
    operator fun inc() = this + 1
    operator fun dec() = this - 1

    infix operator fun times(a: ComplexNum) = ComplexNum(this.real * a.real - this.imag * a.imag, this.real * a.imag + this.imag * a.real)
    infix operator fun div(a: ComplexNum) = this * a.conjugate / (a.real * a.real + a.imag * a.imag)
    infix operator fun times(a: Number) = ComplexNum(this.real * a.toDouble(), this.imag * a.toDouble())
    infix operator fun div(a: Number) = ComplexNum(this.real / a.toDouble(), this.imag / a.toDouble())

    override fun equals(other: Any?) = if (other is ComplexNum) other.real == this.real && other.imag == this.imag else false
    override fun compareTo(other: ComplexNum): Int = (this.abs - other.abs).toInt()
    override fun hashCode(): Int = 31 * real.hashCode() + imag.hashCode()

    override fun toDouble(): Double {
        if (this.imag != 0.0)
            throw ImaginaryPartException(this)
        return this.real
    }

    override fun toByte(): Byte = this.toDouble().toByte()
    override fun toChar(): Char = this.toDouble().toChar()
    override fun toFloat(): Float = this.toDouble().toFloat()
    override fun toInt(): Int = this.toDouble().toInt()
    override fun toLong(): Long = this.toDouble().toLong()
    override fun toShort(): Short = this.toDouble().toShort()

    class ImaginaryPartException(val cn: ComplexNum) : Exception() {
        override fun toString(): String {
            return "ComplexNum $cn contains imaginary part"
        }
    }
}

fun Number.toComplexNum() = ComplexNum(this)
infix operator fun Number.plus(a: ComplexNum) = a + this
infix operator fun Number.minus(a: ComplexNum) = -a + this
infix operator fun Number.times(a: ComplexNum) = a * this
infix operator fun Number.div(a: ComplexNum) = ComplexNum(this) / a