import java.util.Arrays;
import java.util.LinkedList;

public class CommandLine {

    public static String escapeCommandLine(String[] args) {
        StringBuilder res = new StringBuilder();

        for (String el : args) {
            el = el.replaceAll("\"", "\\\\\"");
            if (el.contains(" "))
                el = "\"" + el + "\"";
            res.append(el).append(" ");
        }

        String result = res.substring(0, res.length() - 1); // removing last space

        return result;
    }

    public static String[] parseCommandLine(String cmdline) {
        LinkedList<String> res = new LinkedList<>(); // list for result
        char[] cmdLineArr = cmdline.toCharArray(); // converting cmdLine to char array
        StringBuilder cur = new StringBuilder(); // current arg
        boolean f = false; // true if in quotes
        char last = 0; // last char

        for (char c : cmdLineArr) {
            switch (c) {
                case '"':
                    if (last != '\\')
                        f = !f;
                    else {
                        cur.deleteCharAt(cur.length() - 1);
                        cur.append(c);
                    }
                    break;
                case ' ':
                    if (f)
                        cur.append(c);
                    else {
                        String toAdd = cur.toString();
                        res.add(toAdd);
                        cur = new StringBuilder("");
                    }
                    break;
                default:
                    cur.append(c);
            }
            last = c;
        }
        res.add(cur.toString());

        return res.toArray(new String[]{});
    }

    public static void main(String[] args) {
        System.out.println("args:    " + Arrays.toString(args));

        if (!check(args)) {
            System.err.println("check FAILED");
            System.exit(1);
        }
    }

    private static boolean check(String[] args) {
        String cmdline = escapeCommandLine(args);
        System.out.println("cmdline: <" + cmdline + ">");

        String[] check = parseCommandLine(cmdline);
        System.out.println("check:   " + Arrays.toString(check));

        return Arrays.equals(args, check);
    }
}
